<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/3/2017
 * Time: 6:38 PM
 */

echo "<pre>";

    $myString = "hello world how's life!";
    $myString = addslashes($myString);
    echo $myString;
    echo "<br>";

    $myString = explode(" ",$myString);
    print_r ($myString);
    echo "<br>";

    echo implode(",",$myString);
    echo "<br>";

    print(htmlentities("<br> means line break"));

    $myString = "\n\n\n\nhello\n\n\n\n\";
    $myString = trim($myString);

    $myString2 = 'hello world';
    echo str_pad($myString2,20,'#');


echo "</pre>";
