<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/3/2017
 * Time: 5:49 PM
 */

echo "<pre>";

    //NAMESPACE app;

    define("MYCONSTANT","it's my constant value!");
    echo MYCONSTANT. "<br>".__LINE__;
    echo "<br>";


    echo __LINE__."<br>";
    echo __LINE__."<br>";
    echo __LINE__."<br>";
    echo __LINE__."<br>";

    echo __FILE__;
    echo "<br>";

    function myFunction()
    {
        echo __FUNCTION__;
    }
    myFunction();
    echo "<br>";
    //echo __NAMESPACE__;

    class MyClass{
        public $className;
        public function myMethod(){
            $this->className = __CLASS__;
            echo __METHOD__;
            echo "<br>";
        }
    }

    $myObject = new MyClass();
    $myObject->myMethod();
    echo $myObject->className;
    echo "<br>";

    echo __DIR__;
    echo "<br>";






echo "</pre>";